import React from "react";
import { Helmet } from "react-helmet";

import { Heading, Text } from "../components/Text";
import { TextWrapper } from "../components/Container/ContentWrapper";

export default function Privacy() {
  return (
    <TextWrapper>
      <Helmet>
        <title>Waldorf heute für morgen - Datenschutz</title>
        <meta
          name="description"
          content="Informationen zum Datenschutz der Tagungswebsite „Waldorf heute für morgen“"
        />
      </Helmet>
      <Heading>Datenschutz</Heading>
      <Text>
        Siehe Datenschutzerklärung des Bund der Freien Waldorfschulen e.V. <br /> <br />
        <a href="https://www.waldorfschule.de/datenschutzerklaerungen/#main-content">
          Hier klicken
        </a>
      </Text>
    </TextWrapper>
  );
}
